# Census income classification with scikit-learn
Example repository that can be used as a reference to deploy a trained Scikit-learn model using Deeploy. The explainer is a SHAP Kernel.

## Using this repository in Deeploy Cloud
This example repository is added by default in the first workspace for all Deeploy Cloud users.
Perform the following steps to complete the Deeploy Quickstart (as also described [here](https://docs.deeploy.ml/quickstart)):
* Navigate to **Deployments** and click **Create**.
* Choose the existing Repository option. Select the Repository, the master branch, and the latest commit.
* Choose a name for your Deployment and optionally add a description. Other metadata can be adjusted in the `metadata.json` file at the root of the folder you are deploying from (see [this article](https://docs.deeploy.ml/repository/metadata) for more information).
* In the next step, select Scikit-Learn as the model framework
* In the next step, select Shap kernel as the explainer framework
* In the next step, check your configuration and click **Deploy**
* You will be redirected to the deployment **Events** screen, click on the active event to see more details about the progress of your deployment
* When the deployment is ready, you can test an interaction with the new deployment by navigating to the **Test** tab. Click **Use example input** to make a prediction with the example input from the `metadata.json`. When clicking **Make Prediction** the data is used to inference the deployment.
* View the details of your prediction by navigating to the Predictions tab (only available when you selected to save the prediction in the previous step).
* View documentation of your model by navigation to the Details tab. You can optionally add extra documentation to the model card by adjusting the `model-card.md` file at root of the repository and commiting the changes. After updating your deployment to the latest commit the updates are visable in the UI.

## Training processs
This example uses the standard [Adult (Census-Income) dataset](https://archive.ics.uci.edu/ml/datasets/Adult) from the UCI machine learning data repository. We train a k-nearest neighbors classifier using sci-kit learn and then explain the predictions using Shap.

> Be aware that this dataset is not suitable for real world ML applications. For more info have a look at [this blog post](https://koaning.io/posts/just-another-dangerous-situation/). This dataset is used as an example here because it magnafies the usefulness of explainability.

## Limitation
Because the artifacts in this repository are stored in AWS S3 it is currently only possible to use this example with AWS Private Cloud installations or Deeploy Cloud. When deploying this example on Bare metal, MS Azure and GCP installations it will fail to authenticate with the public s3 bucket 🤯.

