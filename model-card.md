# Credit-scoring Model Card
* **First release**: August 2022
* **Last updated:** October 2023

Inspired by [Model Cards for Model Reporting (Mitchell et al.)](https://arxiv.org/abs/1810.03993).

## Model Details
* **Framework library (version):** Scikit-learn (v1.0.1)
* **Algorithm:** KNN
* **Model type:** Tabular Classifier 


## Explainer Details
* **Framework library (version):** Shap (v0.36.0)
* **Algorithm:** Shap Kernel
* **Explainer type:** Tabular feature importance

The median of the training data is used as background data set in the explainer.
Therefore shap will replace a missing feature value with the median of tha feature.

## Model Use
The model classifies persons in two categories: Income > $50.000,- and Income <= $50.000. In this sample we use the same categories for positive and negative credit scores. The catogory Income > $50.000,- leads to a positive credit score for a loan application and persons in the other category receive a negative credit score.

## Data 
[Income dataset](https://archive.ics.uci.edu/dataset/20/census+income) from UCI, sometimes referred to as the "Adult" dataset.

20% of the training data set is used as a test set.

### Ethical Considerations
Due to the ethical consideration, sex and race have been removed from the model to avoid usage of sensitive features in the model.

## Performance 
Accuracy = 0.8501
Recall = 0.6332
Precision = 0.7105
F1 = 0.6696

## How to use Deeploy
Please use this [Deeploy Documentation](https://docs.deeploy.ml/).

