# Trains model and explainer, and saves them to S3

import datetime
import json
import os.path
from io import BytesIO

import boto3
import dill
import joblib
import numpy as np
import shap
import sklearn


def main():
    # Load data
    X, y = shap.datasets.adult()

    # Filter columns, removing sensitive features
    sensitive_features = ["Sex", "Race"]
    X = X.drop(columns=sensitive_features)

    X_train, X_valid, y_train, y_valid = sklearn.model_selection.train_test_split(
        X, y, test_size=0.2, random_state=7
    )

    # Train knn
    knn = sklearn.neighbors.KNeighborsClassifier()
    knn.fit(X_train, y_train)

    # Generate statistics
    train_feature_stats = get_features_distribution(X_train)

    # Save metadata
    if os.path.isfile("metadata.json"):
        with open("metadata.json", "r", encoding="utf-8") as f:
            metadata = json.load(f)
            metadata["features"] = train_feature_stats["features"]
    else:
        metadata = train_feature_stats

    with open("metadata.json", "w", encoding="utf-8") as f:
        json.dump(metadata, f, ensure_ascii=False, indent=4)

    # Fit Shap Kernel Explainer
    f = lambda x: knn.predict_proba(x)[:, 1]
    med = X_train.median().values.reshape((1, X_train.shape[1]))
    explainer = shap.KernelExplainer(f, med)

    # Export model objects
    timestamp_now = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    bucket_model = "deeploy-examples"
    bucket_explainer = "deeploy-examples"
    explainer_object_key = f"sklearn/census/{timestamp_now}/explainer/explainer.dill"
    model_object_key = f"sklearn/census/{timestamp_now}/model/model.joblib"

    # Setup AWS session named 'deeploy' before running this script
    boto3.setup_default_session(
        profile_name="deeploy"
    )  # https://stackoverflow.com/a/33395432/9994398
    s3 = boto3.client("s3")

    def object_to_s3(bucket, key, object, object_type):
        buffer = BytesIO()
        if object_type == "explainer":
            dill.dump(object, buffer)
        else:
            joblib.dump(object, buffer)
        buffer.seek(0)
        sent_buffer = s3.put_object(Bucket=bucket, Key=key, Body=buffer)
        return sent_buffer["ResponseMetadata"]

    object_to_s3(bucket_model, model_object_key, knn, "model")
    object_to_s3(bucket_explainer, explainer_object_key, explainer, "explainer")

    # Add reference to repo
    model_reference = {
        "reference": {
            "blob": {
                "url": "s3://" + bucket_explainer + "/sklearn/census/" + timestamp_now + "/model"
            }
        }
    }
    explainer_reference = {
        "reference": {
            "blob": {
                "url": "s3://"
                + bucket_explainer
                + "/sklearn/census/"
                + timestamp_now
                + "/explainer"
            }
        }
    }

    with open("model/reference.json", "w", encoding="utf-8") as f:
        json.dump(model_reference, f, ensure_ascii=False, indent=4)
    with open("explainer/reference.json", "w", encoding="utf-8") as f:
        json.dump(explainer_reference, f, ensure_ascii=False, indent=4)


def get_features_distribution(df):
    hist_data = []
    for column in df.columns:
        # Calculate histogram data

        num_unique_values = len(np.unique(df[column]))

        if num_unique_values <= 10:
            num_bins = num_unique_values - 1
        else:
            # Sturges' formula capped at 30 bins
            num_bins = min(30, int(np.ceil(np.log2(len(df[column])) + 1)))

        hist, bin_edges = np.histogram(df[column].dropna(), bins=num_bins)

        distribution_data = []

        for i, count in enumerate(hist):
            distribution_data.append(
                {
                    "start": str(np.round(bin_edges[i], 4)),
                    "end": str(np.round(bin_edges[i + 1], 4)),
                    "count": str(count),
                }
            )

        column_data = {
            "name": column,
            "observedMin": str(df[column].min()),
            "observedMax": str(df[column].max()),
            "observedMean": str(df[column].mean()),
            "distribution": distribution_data,
        }
        hist_data.append(column_data)

    features_obj = {"features": hist_data}
    return features_obj


if __name__ == "__main__":
    main()
