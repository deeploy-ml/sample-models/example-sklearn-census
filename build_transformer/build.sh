#!/usr/bin/env bash
echo "Attempting Docker build and deploy"
read -p "Enter dockerhub user: " dockerhub




read -p "Enter dockerhub repo for transformer: [example-sklearn-census] " transformer_repo
transformer_repo=${transformer_repo:-example-sklearn-census}





echo "...... Building transformer ......"
docker build  . -f job_transformer/Dockerfile -t "$dockerhub"/"$transformer_repo":1.0.2





echo "...... Pushing transformer artifact......"
docker push "$dockerhub"/"$transformer_repo":1.0.2

