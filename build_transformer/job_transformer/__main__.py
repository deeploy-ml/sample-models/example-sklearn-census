from deeploy.cli.deploy_instances.deeploy_transformer import DeeployTransformerLoader 
from transformer import BasicTransformer, InstancesTransformer


def main():
    test_transformer_core = DeeployTransformerLoader()

    test_transformer_core.transformer_serve(
        BasicTransformer, InstancesTransformer
    )


if __name__ == "__main__":
    main()