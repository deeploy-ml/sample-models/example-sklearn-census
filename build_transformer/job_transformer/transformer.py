import logging
import os
import random
from datetime import UTC, datetime, timedelta, timezone
from typing import Dict, Union

import kserve  # To enable logging
from deeploy.cli.wrappers.transformer_wrapper import (
    JobSchedulesInstancesTransformerWrapper,
    TransformerWrapper,
)

logging.basicConfig(level=kserve.constants.KSERVE_LOGLEVEL)
logger = logging.getLogger(__name__)


# Only bare minimum implementation of the transformer is required, since we focus on the instances transformer
class BasicTransformer(TransformerWrapper):
    def __init__(
        self,
    ):
        super().__init__()

    def _preprocess(self, payload: Dict) -> Dict:
        # NOTE: In this case there is no transformation, return payload
        return payload

    def _postprocess(self, response: Dict) -> Dict:
        # NOTE: In this case there is no transformation, return response
        return response


class InstancesTransformer(JobSchedulesInstancesTransformerWrapper):
    def __init__(
        self,
    ):
        """Initializes the Deeploy Model Class"""
        super().__init__()

    def load(self) -> bool:
        return True

    def _predict(self, payload, headers: Dict[str, str] = None):
        """
        Parameters:
            payload (Dict|InferRequest): Prediction inputs.
            headers (Dict): Request headers.

        Returns:
            Dict|InferResponse: Return the prediction result.
        """
        return self.get_random_adult_census_data()
    
    def get_random_adult_census_data(self):
        """
        Returns a random instance of Adult Census data.
        """
        instances = []
        for _ in range(random.randint(2, 50)):
            instances.append(self.get_random_instance())
            
        payload = {
            "instances": instances
        }
        return payload
        
    def get_random_instance(self):
        """
        Returns a random instance of Adult Census data.
        """
        # Once in a while return an empty instance
        if random.randint(0, 10000) > 9990:
            return []
        
        return [
            random.randint(13, 110), 
            random.randint(0, 6), 
            random.randint(4, 15),
            random.randint(0, 6),
            random.randint(0, 14),
            random.randint(0, 5),
            21000 * random.randint(0, 6),
            1000 * random.randint(0, 6),
            random.randint(0, 80),
            random.randint(2, 39)
        ]
