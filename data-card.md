# Credit-scoring Data Card
* **Dataset publish date**: 30-04-1996

## Adult Census Income Dataset
The following was retrieved from [UCI machine learning repository](https://archive.ics.uci.edu/dataset/2/adult).

This data was extracted from the 1994 Census bureau database by Ronny Kohavi and Barry Becker (Data Mining and Visualization, Silicon Graphics). A set of reasonably clean records was extracted using the following conditions: ((AAGE>16) && (AGI>100) && (AFNLWGT>1) && (HRSWK>0)). The prediction task is to determine whether a person makes over $50K a year.

## Dataset details
**Dataset Characteristics**: Multivariate  
**Subject Area**: Social Science  
**Associated Task**: Classification  
**Feature Type**: Categorical, Integer  
**Number of Instances**: 48842  
**Number of Features**: 14

## Features
| Variable Name | Role | Type | Demographic | Description | Missing Values |
|---|---|---|---|---|---|
| age  | Feature  | Integer  | Age  | N/A  | no  |
| workclass | Feature  | Categorical  | Income | Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked | yes |
| fnlwgt | Feature | Integer |  |  | no |
| education | Feature  | Categorical | Education Level | Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool | no |
| education-num | Feature | Integer | Education Level |   | no |
| marital-status | Feature | Categorical | Other | Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse | no |
| occupation | Feature | Feature | Other | Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces | yes |
| relationship | Feature | Categorical | Other | Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried | no |
| race | Feature | Categorical | Race | Omitted | no |
| sex | Feature | Binary | Sex | Omitted | no |
| capital-gain | Feature | Integer |   |   | no |
| capital-loss | Feature | Integer |   |   | no |
| hours-per-week | Feature | Integer |   |   | no |
| native-country | Feature | Categorical | Other | United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands | yes |
| income | Target | Binary | Income |   |  no |


## Description of fnlwgt (final weight)

The weights on the Current Population Survey (CPS) files are controlled to independent estimates of the civilian noninstitutional population of the US. These are prepared monthly for us by Population Division here at the Census Bureau. We use 3 sets of controls. These are:

A single cell estimate of the population 16+ for each state.
Controls for Hispanic Origin by age and sex.
Controls by Race, age and sex.
We use all three sets of controls in our weighting program and "rake" through them 6 times so that by the end we come back to all the controls we used. The term estimate refers to population totals derived from CPS by creating "weighted tallies" of any specified socio-economic characteristics of the population. People with similar demographic characteristics should have similar weights. There is one important caveat to remember about this statement. That is that since the CPS sample is actually a collection of 51 state samples, each with its own probability of selection, the statement only applies within state.  
